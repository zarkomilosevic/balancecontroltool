class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  include SessionsHelper

  def logged_in_user
    if(current_user.nil?)
      redirect_to login_path
    end
  end

  def correct_user?(user_id)
    if(current_user.present?)
      redirect_to current_user if user_id.to_i != current_user.id
    end
  end

end
