class PagesController < ApplicationController
  def home
    if(is_logged_in?)
      @calculation = current_user.calculations.build
      @calculations = current_user.calculations
    end
  end
end
