require 'calculation_runner'
class CalculationsController < ApplicationController

  before_action :correct_user, only: [:show, :destroy, :show_original_image, :kill_calculation_process]
  before_action :logged_in_user

  def create
    @calculation = current_user.calculations.build(calculation_params)
    if(@calculation.save)
      CalculationRunner.calculate(@calculation)
      redirect_to root_path
    else
      @calculations = current_user.calculations
      render 'pages/home'
    end
  end

  def show
    @calculation = Calculation.find_by(id: params[:id])
  end

  def show_original_image
    @calculation = Calculation.find_by(id: params[:id])
  end

  def destroy
    @calculation = Calculation.find_by(id: params[:id])
    if(@calculation.present?)
      CalculationRunner.delete_calculation_data(@calculation)
      @calculation.destroy
      redirect_to root_path
    end
  end

  def kill_calculation_process
    @calculation = Calculation.find_by(id: params[:id])
    if(@calculation.present?)
      CalculationRunner.kill_calculation_process(@calculation)
      @calculation.destroy
      redirect_to root_path
    end
  end

  private

  def correct_user
    user = Calculation.find_by(id: params[:id]).user
    correct_user?(user.id)
  end

  def calculation_params
    params.require(:calculation).permit(:bodyWeight, :bodyLength, :inputFile)
  end
end
