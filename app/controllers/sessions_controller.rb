require 'sessions_helper'
class SessionsController < ApplicationController

  def new
    render 'new'
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if(user && user.authenticate(params[:session][:password]))
      log_in(user)
      redirect_to root_path
    else
      flash.now[:danger] = "Wrong email or password!"
      render 'new'
    end
  end

  def destroy
    if(is_logged_in?)
      log_out
      redirect_to root_path
    end
  end

end
