class Calculation < ApplicationRecord

  belongs_to :user
  mount_uploader :inputFile, FileUploader

  serialize :mass,                          Array
  serialize :height,                        Array
  serialize :inertia,                       Array
  serialize :omega,                         Array
  serialize :beta,                          Array
  serialize :proportional_gain,             Array
  serialize :derivative_gain,               Array
  serialize :time_delay,                    Array
  serialize :sensory_weighting_parameter,   Array
  serialize :tau,                           Array
  serialize :force_gain,                    Array
  serialize :gof_body_sway,                 Array
  serialize :gof_torque,                    Array
  serialize :gof_emg,                       Array

  validates :bodyWeight,  presence: true
  validates :bodyLength,  presence: true
  validates :inputFile,   presence: true

end
