# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161003091333) do

  create_table "calculations", force: :cascade do |t|
    t.string   "bodyWeight"
    t.string   "bodyLength"
    t.string   "inputFile"
    t.integer  "user_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "jobStatus",                   default: false
    t.integer  "pid"
    t.text     "mass"
    t.text     "height"
    t.text     "inertia"
    t.text     "omega"
    t.text     "beta"
    t.text     "proportional_gain"
    t.text     "derivative_gain"
    t.text     "time_delay"
    t.text     "sensory_weighting_parameter"
    t.text     "tau"
    t.text     "force_gain"
    t.text     "gof_body_sway"
    t.text     "gof_torque"
    t.text     "gof_emg"
    t.index ["user_id"], name: "index_calculations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
