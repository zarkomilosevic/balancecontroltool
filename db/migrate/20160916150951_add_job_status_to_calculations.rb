class AddJobStatusToCalculations < ActiveRecord::Migration[5.0]
  def change
    add_column :calculations, :jobStatus, :boolean, default: false
  end
end
