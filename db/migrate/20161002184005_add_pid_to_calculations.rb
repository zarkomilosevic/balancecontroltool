class AddPidToCalculations < ActiveRecord::Migration[5.0]
  def change
    add_column :calculations, :pid, :integer
  end
end
