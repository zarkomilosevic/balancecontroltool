class CreateCalculations < ActiveRecord::Migration[5.0]
  def change
    create_table :calculations do |t|
      t.string :bodyWeight
      t.string :bodyLength
      t.string :inputFile
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
