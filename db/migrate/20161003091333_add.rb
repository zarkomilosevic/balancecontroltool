class Add < ActiveRecord::Migration[5.0]
  def change
    add_column :calculations, :mass,                          :text
    add_column :calculations, :height,                        :text
    add_column :calculations, :inertia,                       :text
    add_column :calculations, :omega,                         :text
    add_column :calculations, :beta,                          :text
    add_column :calculations, :proportional_gain,             :text
    add_column :calculations, :derivative_gain,               :text
    add_column :calculations, :time_delay,                    :text
    add_column :calculations, :sensory_weighting_parameter,   :text
    add_column :calculations, :tau,                           :text
    add_column :calculations, :force_gain,                    :text
    add_column :calculations, :gof_body_sway,                 :text
    add_column :calculations, :gof_torque,                    :text
    add_column :calculations, :gof_emg,                       :text
  end
end
