require_relative 'publisher'
require 'fileutils'
require 'open3'
require 'ImageResize'
require 'json'

class CalculationRunner
  include Publisher

  EXE_NAME                                  = 'BalanceControlTool.exe'
  OUTPUT_RESULTS_FILE_NAME                  = 'OutputMeasurement.js'
  OUTPUT_IMAGE_FULL_SIZE_FILE_NAME          = 'OutputFRFimage_all.png'
  OUTPUT_IMAGE_REDUCED_SIZE_FILE_NAME       = 'OutputFRFimage_all_display.png'

  INPUT_BODY_MEASURES_FILE_NAME = 'InputBodyMeasures.txt'
  INPUT_MEASUREMENT_FILE_NAME   = 'InputMeasurement.txt'

  EXE_PATH = Rails.root.join('public', 'uploads', 'calculation', EXE_NAME)

  def self.calculations_folder_path
    Rails.root.join('public', 'uploads', 'calculation', 'inputFile')
  end

  def self.calculation_folder_path(record)
    CalculationRunner.calculations_folder_path.join(record.id.to_s)
  end

  def self.calculation_output_path(record, fileName)
    File.join('/uploads', 'calculation', 'inputFile', record.id.to_s, fileName)
  end

  def self.calculation_output_results_path(record)
    CalculationRunner.calculation_output_path(record, OUTPUT_RESULTS_FILE_NAME)
  end

  def self.calculation_output_image_path(record, fullSize = false)
    if(fullSize)
      CalculationRunner.calculation_output_path(record, OUTPUT_IMAGE_FULL_SIZE_FILE_NAME)
    else
      CalculationRunner.calculation_output_path(record, OUTPUT_IMAGE_REDUCED_SIZE_FILE_NAME)
    end
  end

  def self.calculation_exe_path(record)
    CalculationRunner.calculation_folder_path(record).join(EXE_NAME)
  end

  def self.delete_calculation_data(record)
    FileUtils.remove_dir(CalculationRunner.calculation_folder_path(record), true)
  end

  def self.kill_calculation_process(record)
    if(record.pid.present?)
      system("taskkill /PID #{record.pid} /f")
      CalculationRunner.delete_calculation_data(record)
    end
  end

  def self.write_input_file(record)
      f = File.open(calculation_folder_path(record).join(INPUT_BODY_MEASURES_FILE_NAME), "a")
      f.puts("Body Weight: #{record.bodyWeight.to_s}")
      f.puts("Body Length: #{record.bodyLength.to_s}")
      f.close
  end

  def self.resize_image(record)
    Image.resize(CalculationRunner.calculation_folder_path(record).join(OUTPUT_IMAGE_FULL_SIZE_FILE_NAME),
                 CalculationRunner.calculation_folder_path(record).join(OUTPUT_IMAGE_REDUCED_SIZE_FILE_NAME),
                 1600, 1200)
  end

  def self.store_calculation_results_txt(file, record)
    record.update_attribute(:jobStatus, true)


    # Mass: 59.4252      59.4252      59.4252      59.4252
    res = file.gets.split[1..4]
    record.mass = res;
    #Height: 0.95994     0.95994     0.95994     0.95994
    res = file.gets.split[1..4]
    record.height = res;
    #Inertia: 63.0937      63.0937      63.0937      63.0937
    res = file.gets.split[1..4]
    record.inertia = res;
    #Omega: 42.5383       34.656      33.8591      15.9826
    res = file.gets.split[1..4]
    record.omega = res;
    #Beta: 1.7699      1.6788       1.539     0.47266
    res = file.gets.split[1..4]
    record.beta = res;
    #Proportional gain (normalized): 1.0043      1.2894      1.1485      1.2587
    res = file.gets.split[3..6]
    record.proportional_gain = res;
    #Derivative gain (normalized): 0.49744     0.46083     0.50227       0.437
    res = file.gets.split[3..6]
    record.derivative_gain = res;
    #Time_delay: 0.13787     0.10004     0.13151     0.11257
    res = file.gets.split[1..4]
    record.time_delay = res;
    #Sensory weighting parameter: 0.47602     0.45668     0.61995     0.55458
    res = file.gets.split[3..6]
    record.sensory_weighting_parameter = res;
    #Tau: 2.93527      11.1479      5.47093      4.78413
    res = file.gets.split[1..4]
    record.tau = res;
    #Force_gain: 0.001163   0.0020906   0.0016474   0.0011795
    res = file.gets.split[1..4]
    record.force_gain = res;
    #Empty array
    file.gets
    #GOF body sway: 48.387      26.9428      85.5604      85.8213
    res = file.gets.split[3..6]
    record.gof_body_sway = res;
    #GOF torque: 42.1702      39.2729      26.7837      42.2868
    res = file.gets.split[2..5]
    record.gof_torque = res;
    #GOF EMG: -2.20702      2.50565      15.9257      32.6265
    res = file.gets.split[2..5]
    record.gof_emg = res;

    record.save
  end

  def self.store_calculation_results_json(file, record)
    data_hash = JSON.parse(file)

    record.update_attribute(:jobStatus, true)

    record.mass                           = data_hash['mass'].values
    record.height                         = data_hash['height'].values
    record.inertia                        = data_hash['inertia'].values
    record.omega                          = data_hash['omega'].values
    record.beta                           = data_hash['beta'].values
    record.proportional_gain              = data_hash['proportionalGainNormalized'].values
    record.derivative_gain                = data_hash['derivativeGainNormalized'].values
    record.time_delay                     = data_hash['timeDelay'].values
    record.sensory_weighting_parameter    = data_hash['sensoryWeightingParameter'].values
    record.tau                            = data_hash['tau'].values
    record.force_gain                     = data_hash['forceGain'].values
    record.gof_body_sway                  = data_hash['gOFBodySway'].values
    record.gof_torque                     = data_hash['gOFTorque'].values
    record.gof_emg                        = data_hash['gOFEMG'].values

    record.save

  end

  def self.calculate(record)
    Thread.new do
      FileUtils.cp(EXE_PATH, CalculationRunner.calculation_folder_path(record))
      write_input_file(record)
      Dir.chdir(CalculationRunner.calculation_folder_path(record)) do
        #system(EXE_NAME)
        pid = spawn(EXE_NAME)
        record.update_attribute(:pid, pid)
        Process.wait pid

        FileUtils.remove_file(calculation_exe_path(record))
        #FileUtils.cp(CalculationRunner.calculation_folder_path(record).join(OUTPUT_IMAGE_FULL_SIZE_FILE_NAME), CalculationRunner.calculation_folder_path(record).join("Bak.jpg"))
        CalculationRunner.resize_image(record)

        file = File.read(OUTPUT_RESULTS_FILE_NAME)
        CalculationRunner.store_calculation_results_json(file, record)
      end
      #system("cd #{CalculationRunner.calculation_folder_path(record).to_s} && #{EXE_NAME}")

      ActiveRecord::Base.connection.close
    end
  end
end