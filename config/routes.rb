Rails.application.routes.draw do

  get 'calculations/new'

  root 'pages#home'
  get     '/signup',    to: 'users#new'

  get     '/login',     to: 'sessions#new'
  post    '/login',     to: 'sessions#create'
  delete  '/logout',    to: 'sessions#destroy'

  resources :users

  resources :calculations, only: [:create, :show, :destroy] do
    member do
      get :show_original_image
      get :kill_calculation_process
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
