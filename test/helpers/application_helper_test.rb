
class ApplicationHelperTest < ActiveSupport::TestCase

  test 'full title' do
    base_title = 'BalanceControlTool'
    title = 'Home'
    assert_equal base_title, full_title
    assert_equal title + ' | ' + base_title, full_title(title)
  end

end