require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
  end

  test 'home page layout' do
    get root_path

    assert_template 'pages/home'
    assert_template 'layouts/_header'
    assert_template 'layouts/application'

    assert_select 'title', full_title("Home")
    assert_select 'a[href=?]', root_path, count: 2
    assert_select 'a[href=?]', signup_path, count: 2
    assert_select 'a[href=?]', login_path

    log_in_as(@user)
    assert_redirected_to root_path
    follow_redirect!

    assert_template 'pages/home'
    assert_template 'shared/_form_calculations'
  end

  test 'new user page layout' do

    get signup_path
    assert_template 'users/new'
    assert_template 'users/_form'
    assert_template 'shared/_error_messages'

    assert_select 'title', full_title("Create account")
    #assert_select 'a[href=?]', users_path

  end

  test 'show user page layout' do
    get user_path(users(:michael))
  end

  test 'log in page layout' do
    get login_path
    assert_template 'sessions/new'
    assert_select 'title', full_title('Log in')
  end

end
