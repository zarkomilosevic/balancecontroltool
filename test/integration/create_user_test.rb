require 'test_helper'

class CreateUserTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test 'test layout in create user actions' do

    get new_user_path
    assert_select 'a[href=?]', login_path
    assert_select 'a[href=?]', signup_path
    # invalid user should be redirected to new page
    post users_path params: { user: { name: "Zarko",
                                      email: "zarko@kg.ac.rs",
                                      password: "foobar",
                                      password_confirmation: "foobaz" } }

    assert_template 'users/new'
    assert_template 'shared/_error_messages'

    post users_path params: { user: { name: "Zarko",
                                      email: "zarko@kg.ac.rs",
                                      password: "foobar",
                                      password_confirmation: "foobar" } }

    assert is_logged_in?
    assert_equal User.last, current_user

    assert_redirected_to root_path
    follow_redirect!
    assert_template 'pages/home'
    assert_template 'shared/_form_calculations'

    assert_select 'a[href=?]', login_path, count: 0
    assert_select 'a[href=?]', signup_path, count: 0

    assert_select 'a[href=?]', user_path(current_user)
    assert_select 'a[href=?]', edit_user_path(current_user)
    assert_select 'a[href=?]', logout_path

  end
end
