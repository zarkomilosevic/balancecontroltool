require 'test_helper'

class UserLoginTestTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
  end

  test 'user login change header layout' do

    get login_path
    assert_select 'a[href=?]', login_path
    assert_select 'a[href=?]', signup_path

    log_in_as(@user)
    assert_redirected_to root_path
    follow_redirect!
    assert_template 'shared/_form_calculations'
    assert is_logged_in?

    assert_select 'a[href=?]', login_path, count: 0
    assert_select 'a[href=?]', signup_path, count: 0

    assert_select 'a[href=?]', user_path(@user)
    assert_select 'a[href=?]', edit_user_path(@user)
    assert_select 'a[href=?]', logout_path

  end
end
