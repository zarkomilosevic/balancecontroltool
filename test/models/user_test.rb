require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.new( name: "Zarko Milosevic",
                      email: "zarko@kg.ac.rs",
                      password: "foobar",
                      password_confirmation: "foobar" )
  end

  test 'user should be vaild' do
    assert @user.valid?
  end

  test 'name should be present' do
    @user.name = "     "
    assert_not @user.valid?
  end

  test 'user should have name with maximum length of 50 characters' do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test 'email should be present' do
    @user.email = "     "
    assert_not @user.valid?
  end

  test 'user should have email with maximum length of 255 characters' do
    @user.email = "a" * 260
    assert_not @user.valid?
  end

  test 'email validation should accept valid email adresses' do
    valid_adresses =  %w[ser@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]

    valid_adresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect}, should be valid"
    end

  end

  test 'email validation should reject invalid email adresses' do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]

    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address}, should not be valid"
    end
  end

  test 'email should be unique' do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test 'email should be case insensitive' do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test 'email should be saved with lowercase' do
    email_upper = "FOOBAR@EMAIL.COM"
    @user.email = email_upper
    @user.save
    assert_equal email_upper.downcase, @user.reload.email
  end

  test 'password should be present' do
    @user.password = "     "
    assert_not @user.valid?
  end

  test 'user should have password with minimum length of 6 characters' do
    @user.password = "a" * 5
    assert_not @user.valid?
  end

  test 'password and confirmation have to match' do
    @user.password = "foobar"
    @user.password_confirmation = 'foobaz'
    assert_not @user.valid?
  end

end
