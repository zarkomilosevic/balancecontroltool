require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "should get new" do
    get login_path
    assert_response :success
  end

  test 'create session' do
    post login_path, params: { session: { email: @user.email,
                                          password: 'password' } }

    assert_equal @user, current_user
    assert is_logged_in?

    assert_redirected_to root_path
  end

end
