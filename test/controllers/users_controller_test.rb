require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test 'create should render new with invalid user' do
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "Zarko Milosevic",
                                         email: "zarko@kg.ac.rs",
                                         password: "foobar",
                                         password_confirmation: "foobaz" } }
    end
    assert_template 'users/new'
  end

  test 'create should redirect valid user to user page' do

    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name: 'Zarko Milosevic',
                                        email: 'zarko@kg.ac.rs',
                                        password: 'foobar',
                                        password_confirmation: 'foobar' } }
    end

    user = assigns(:user)
    assert_redirected_to root_path
    assert_not flash.empty?
  end

  test 'show redirects logged user to show page' do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
  end

  test 'update  redirects logged user with right parameters to show page with wrong parameters renders edit page' do
    log_in_as(@user)
    new_email = 'new@example.com'
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: new_email,
                                              password: 'password',
                                              password_confirmation: 'password' } }
    assert_redirected_to @user
    assert_equal new_email, @user.reload.email

    patch user_path(@user), params: { user: { name: @user.name,
                                              email: new_email,
                                              password: '',
                                              password_confirmation: 'password' } }

    assert_template 'users/edit'
  end

  test 'show redirects non logged or wrong user to login page' do
    get user_path(@user)
    assert_redirected_to login_path

    log_in_as(@other_user)
    get user_path(@user)
    assert_redirected_to login_path
  end

  test 'edit redirects non logged or wrong user to login page' do
    get edit_user_path(@user)
    assert_redirected_to login_path

    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_redirected_to login_path
  end

  test 'update redirects non logged or wrong user to login page' do
    get edit_user_path(@user)
    assert_redirected_to login_path

    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_redirected_to login_path
  end

end
